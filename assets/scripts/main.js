$('.bt-sobre').click(function(){
    //adiciona underline no link clicado
    $(this).addClass('undline');
    $('.bt-contato').removeClass('undline');

    //diminui o logo
    $('.logo').removeClass('engorge');
    $('.logo').addClass('reduce');

    //sobe o menu
    $('.nav').removeClass('down');
    $('.nav').addClass('up');
    
    //deixa apenas o video 2 rodando    
    $('.video1').hide();
    $('.video3').hide();
    $('.video2').show();

    //deixa apenas crop2
    $('.video-wrapper').removeClass('cut1');
    $('.video-wrapper').addClass('cut2');

    //remove box contato
    $('.contato').fadeOut(400);

    //mostra box sobre
    $('.sobre').delay(400).show(0);
    $('.sobre').delay(800).addClass('up');
});

$('.bt-contato').click(function(){
    //adiciona underline no link clicado
    $(this).addClass('undline');
    $('.bt-sobre').removeClass('undline');

    //diminui o logo
    $('.logo').removeClass('engorge');
    $('.logo').addClass('reduce');

    //sobe o menu
    $('.nav').removeClass('down');
    $('.nav').addClass('up');

    //deixa apenas o video 3 rodando
    $('.video1').hide();
    $('.video2').hide();
    $('.video3').show();

    //deixa apenas o crop1
    $('.video-wrapper').removeClass('cut2');
    $('.video-wrapper').addClass('cut1');

    //remove box sobre
    $('.sobre').fadeOut();

    //mostra box contato
    $('.contato').delay(400).show(0);
    $('.contato').delay(800).addClass('up');
});

$('.logo').click(function(){
    //remove linha dos botoes
    $('.bt-contato').removeClass('undline');
    $('.bt-sobre').removeClass('undline');

    //verifica se o logo já tem alguma classe (previne glitch)
    var a = $(this).attr('class');
    if(a !== 'logo'){
        //aumenta logo
        $('.logo').addClass('engorge');
        $('.logo').removeClass('reduce');
        //sobre nav
        $('.nav').removeClass('up');
        $('.nav').addClass('down');
    };
    //deixa apenas o video 1 rodando
    $('.video1').show();
    $('.video2').hide();
    $('.video3').hide();

    //remove o crop do video
    $('.video-wrapper').removeClass('cut1');
    $('.video-wrapper').removeClass('cut2');

    //remove box sobre
    $('.sobre').hide();
    $('.contato').hide();

});

//awesomeness
var kkeys = [], konami = "38,38,40,40,37,39,37,39,66,65";

$(document).keydown(function(e) {

  kkeys.push( e.keyCode );

  if ( kkeys.toString().indexOf( konami ) >= 0 ) {

    $(document).unbind('keydown',arguments.callee);
    
    // do something awesome
    $("body").addClass("konami");
    $(".video-wrapper").hide();
  
  }

});